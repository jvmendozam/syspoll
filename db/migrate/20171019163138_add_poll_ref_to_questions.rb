class AddPollRefToQuestions < ActiveRecord::Migration
  def change
    add_reference :questions, :polls, index: true, foreign_key: true
  end
end
