class AddQuestionRefToAnswers < ActiveRecord::Migration
  def change
    add_reference :answers, :questions, index: true, foreign_key: true
  end
end
