Rails.application.routes.draw do
  get 'manage_answer/edit'

  get 'manage_answer/index'

  get 'manage__questions/new'

  get 'manage__questions/edit'

match 'manage__questions_ajax/edit' => 'manage__questions_ajax#edit', via: :get
match 'manage_answer/index' => 'manage_answer#create', via: :post
match 'manage_answer/edit' => 'manage_answer#update', via: :put
  get 'edit_poll_questions_ajax/index'

  get 'add_questions_ajax/index'
  match 'add_questions_ajax/index' => 'add_questions_ajax#create', via: :post
  match 'edit_poll_questions_ajax/index' => 'edit_poll_questions_ajax#update', via: :put
  resources :add_questions
  get 'make_form/index'
  match '/make_form/index' => 'make_form#add_reference', via: :post
  resources :polls
  resources :answers
  resources :questions
  get 'show_poll/index'
  root 'polls#index', as: 'polls_index', via: :all

 


 

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
