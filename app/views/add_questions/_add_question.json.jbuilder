json.extract! add_question, :id, :created_at, :updated_at
json.url add_question_url(add_question, format: :json)
