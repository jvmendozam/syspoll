class PollsController < ApplicationController
  before_action :set_poll, only: [:show, :edit, :update, :destroy]
    #after_action :testRender , only: :show
  # GET /polls
  # GET /polls.json
  def index
    @polls = Poll.all
    @polls.each do |poll|
      @questions = Question.where("polls_id=?",poll.id)
    end
  end

  # GET /polls/1
  # GET /polls/1.json
  def show
     @questions=Question.where(["polls_id =?", @poll.id])
  end

  # GET /polls/new
  def new
    @poll = Poll.new
  end

  # GET /polls/1/edit
  def edit
    @questions = Question.where("polls_id=?",@poll.id)
  end

  # POST /polls
  # POST /polls.json
  def create
    @poll = Poll.new(poll_params)
    respond_to do |format|
      if @poll.save
        format.html { redirect_to add_questions_ajax_index_path(poll_id: @poll) }
        format.json { render :show, status: :created, location: @poll }
      else
        format.html { render :new }
        format.json { render json: @poll.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /polls/1
  # PATCH/PUT /polls/1.json
  def update
    respond_to do |format|
      if @poll.update(poll_params)
        format.html { redirect_to edit_poll_questions_ajax_index_path(poll_id: @poll) }
        format.json { render :show, status: :created, location: @poll }
      else
        format.html { render :edit }
        format.json { render json: @poll.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /polls/1
  # DELETE /polls/1.json
  def destroy
    @poll.destroy
    respond_to do |format|
      format.html { redirect_to polls_url, notice: 'Poll was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_poll
      @poll = Poll.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def poll_params
      #params.fetch(:poll, {})
      params.require(:poll).permit(:title , :description , :votes)
    end

    def add_question
      @poll=Poll.find(params [:poll_id])
      @question=Question.find(params [:question_id])
      @poll.question_id=@question.id
      if @poll.save
        format.html { redirect_to @poll, notice: 'Poll was successfully updated.' }
        format.json { render :show, status: :ok, location: @poll }
      end
    end
    def testRender (poll)
      if !poll.nil?
        @questions = Question.where("polls_id=?",poll.id)
      end
    end

  end
