class EditPollQuestionsAjaxController < ApplicationController
  def index
  	@poll=Poll.find(params[:poll_id])
  	@questions = Question.where("polls_id=?",@poll.id)
  end

  def update
    @poll=Poll.find(params[:poll_id])
    @deleted_questions_array = JSON.parse(params[:data_value])
    @array_questions = JSON.parse(params[:data_value_add])
    if !@deleted_questions_array.nil?
    	@deleted_questions_array.each do |id|
       Question.destroy(id)
     end
     @array_questions.each do |question|
        Question.create(description: question,polls_id: @poll.id)
      end
   end
   respond_to do |format|
     format.html { redirect_to polls_index_path, notice: 'Poll was successfully updated.' }
        #format.json { render :show, status: :ok, location: @poll }
      end  
    end	
  end