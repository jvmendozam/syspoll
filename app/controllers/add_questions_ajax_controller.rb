class AddQuestionsAjaxController < ApplicationController

  def index
    @poll=Poll.find(params[:poll_id])
  end
  def create 
	    @poll=Poll.find(params[:poll_id])
      @array_questions = JSON.parse(params[:data_value])
      @array_questions.each do |question|
        Question.create(description: question,polls_id: @poll.id)
      end
         respond_to do |format|
        format.html { redirect_to polls_index_path, notice: 'Poll was successfully updated.' }
        #format.json { render :show, status: :ok, location: @poll }
      end
  end
end
