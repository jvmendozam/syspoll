class AddQuestionsController < ApplicationController
  before_action :set_add_question, only: [:show, :edit, :update, :destroy]

  # GET /add_questions
  # GET /add_questions.json
  def index
    @poll=Poll.find(params[:poll_id])
    @add_questions = AddQuestion.all
    @questions=Question.all
    @questions_filter=Question.where("polls_id is null")
  end

  # GET /add_questions/1
  # GET /add_questions/1.json
  def show
  end

  # GET /add_questions/new
  def new
    @add_question = AddQuestion.new
  end

  # GET /add_questions/1/edit
  def edit
  end

  # POST /add_questions
  # POST /add_questions.json
  def create
      @poll=Poll.find(params[:poll_id])
      @array_id = JSON.parse(params[:data_value])
      @array_id.each do |id|
        question=Question.find(id)
        question.polls_id=@poll.id
        question.save
      end
        respond_to do |format|
        format.html { redirect_to polls_index_path, notice: 'Poll was successfully updated.' }
        #format.json { render :show, status: :ok, location: @poll }

      end
  end

  # PATCH/PUT /add_questions/1
  # PATCH/PUT /add_questions/1.json
  def update
    respond_to do |format|
      if @add_question.update(add_question_params)
        format.html { redirect_to @add_question, notice: 'Add question was successfully updated.' }
        format.json { render :show, status: :ok, location: @add_question }
      else
        format.html { render :edit }
        format.json { render json: @add_question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /add_questions/1
  # DELETE /add_questions/1.json
  def destroy
    @add_question.destroy
    respond_to do |format|
      format.html { redirect_to add_questions_url, notice: 'Add question was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_add_question
      @add_question = AddQuestion.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def add_question_params
      params.fetch(:add_question, {})
    end
end
