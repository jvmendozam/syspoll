class ManageAnswerController < ApplicationController
	def index
		@question=Question.find(params[:question_id])
	end

	def create
		debugger
		@question=Question.find(params[:question_id])
		@array_answers = JSON.parse(params[:data_value])
		@array_answers.each do |answer|
			Answer.create(description: answer,questions_id: @question.id)
		end
		respond_to do |format|
			format.html { redirect_to @question, notice: 'Question was successfully updated.' }
        #format.json { render :show, status: :ok, location: @poll }
    end
end


def edit
	@question=Question.find(params[:question_id])
end

def update
	debugger
	@question=Question.find(params[:question_id])
	@deleted_answers_array = JSON.parse(params[:data_value])
	@array_answers = JSON.parse(params[:data_value_add])
	if !@deleted_answers_array.nil?
		@deleted_answers_array.each do |id|
			Answer.destroy(id)
		end
	end
	if !@array_answers.nil?
		@array_answers.each do |answer|
			Answer.create(description: answer,questions_id: @question.id)
		end
	end
	
	respond_to do |format|
		format.html { redirect_to questions_path, notice: 'Answer was successfully updated.' }
        #format.json { render :show, status: :ok, location: @poll }
    end  
end

end
