module PollsHelper

	def test_add
		@poll=Poll.find(params [:poll_id])
	      @question=Question.find(params [:question_id])
	      @poll.question_id=@question.id
	      if @poll.save
	        format.html { redirect_to @poll, notice: 'Poll was successfully updated.' }
	        format.json { render :show, status: :ok, location: @poll }
	    end
	end
end
